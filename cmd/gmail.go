package main

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/mail"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
)

var (
	argUser         = flag.String("user", "me", "Gmail user")
	argShowLabels   = flag.Bool("show-labels", false, "Show labels")
	argSaveMessages = flag.Bool("save-messages", false, "Save messages")
	argTime         = flag.String("time", "24h", "Consider all messages received in the previous N minutes")

	argInputFile  = flag.String("input", "", "Load messages from this file instead of Gmail")
	argOutputFile = flag.String("output", "", "Save results into this file")

	argVerbose = flag.Bool("verbose", false, "Verbose output")
)

var (
	gmailService *gmail.Service
	fromTime     time.Time
)

func main() {
	flag.Parse()

	if *argInputFile != "" {
		showEmailTree()
		return
	}

	dt, err := time.ParseDuration(*argTime)
	if err != nil {
		log.Fatalf("unable to parse time argument: %v", err)
	}
	fromTime = time.Now().Add(-dt)

	if gmailService, err = newGmailService(); err != nil {
		log.Fatalf("unable to create Gmail service: %v", err)
	}

	if *argShowLabels {
		showLabels()
	}
	if *argSaveMessages {
		if err = saveMessages(); err != nil {
			log.Fatalf("unable to save messages: %v", err)
		}
	}
}

func newGmailService() (*gmail.Service, error) {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		return nil, fmt.Errorf("unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, gmail.GmailReadonlyScope)
	if err != nil {
		return nil, fmt.Errorf("unable to parse client secret file to config: %v", err)
	}

	srv, err := gmail.New(getClient(config))
	if err != nil {
		return nil, fmt.Errorf("unable to retrieve Gmail client: %v", err)
	}

	return srv, nil
}

func showLabels() error {
	resp, err := gmailService.Users.Labels.List(*argUser).Do()
	if err != nil {
		return err
	}
	for _, l := range resp.Labels {
		b, err := l.MarshalJSON()
		if err != nil {
			return err
		}
		fmt.Println(string(b))
	}
	return nil
}

func headerValue(headers []*gmail.MessagePartHeader, name string) string {
	for _, h := range headers {
		if h.Name == name {
			return h.Value
		}
	}
	return ""
}

func saveMessages() error {
	f, err := os.Create(*argOutputFile)
	if err != nil {
		return err
	}

	if *argVerbose {
		defer func() {
			fmt.Fprintln(os.Stderr)
		}()
	}

	listCall := gmailService.Users.Messages.List(*argUser)

	w := csv.NewWriter(f)
	n := 0

	errDoneProcessing := errors.New("done processing")

	// NOTE: it's assumed that the messages are recieved ordered by date,
	// beginning from the most recent one.
	//
	err = listCall.Pages(context.Background(),
		func(listMsgResp *gmail.ListMessagesResponse) error {
			for _, m := range listMsgResp.Messages {

				msg, err := gmailService.Users.Messages.Get(*argUser, m.Id).Do()
				if err != nil {
					log.Fatalf("Failed to get a message: %v", err)
				}

				msgDate := time.Unix(msg.InternalDate/1000, 0)
				if msgDate.Before(fromTime) {
					w.Flush()
					if err := w.Error(); err != nil {
						return err
					}
					return errDoneProcessing
				}

				h := msg.Payload.Headers

				var record []string
				record = append(record, msg.Id)
				record = append(record, strconv.FormatInt(msg.InternalDate, 10))
				record = append(record, headerValue(h, "From"))
				record = append(record, headerValue(h, "To"))
				record = append(record, headerValue(h, "Subject"))
				record = append(record, headerValue(h, "Date"))
				record = append(record, msg.ThreadId)
				record = append(record, strings.Join(msg.LabelIds, ":"))
				record = append(record, headerValue(h, "List-Unsubscribe"))
				w.Write(record)
				n++

				if *argVerbose {
					fmt.Fprintf(os.Stderr, "\r%d messages", n)
				}
			}
			return nil
		})

	if err != nil && err != errDoneProcessing {
		f.Close()
		return fmt.Errorf("unable to complete message download: %v", err)
	}

	w.Flush()

	csvErr := w.Error()
	fileErr := f.Close()

	if csvErr != nil {
		return csvErr
	}
	return fileErr
}

func iterateCSVFile(filename string, process func(record []string) bool) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	r := csv.NewReader(f)
	for {
		record, err := r.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		if !process(record) {
			break
		}
	}
	return nil
}

type node struct {
	children map[string]*node
	count    int
	name     string

	// Unsubscribe instructions are available.
	unsubscribe bool
}

func newNode(name string, unsubscribe string) *node {
	unsubscribeInfo := false
	if len(unsubscribe) > 0 {
		unsubscribeInfo = true
	}
	return &node{
		children:    nil,
		count:       0,
		name:        name,
		unsubscribe: unsubscribeInfo,
	}
}

func (n *node) add(names []string, unsubscribe string) {
	n.count++

	if len(names) == 0 {
		return
	}

	name := names[0]

	if n.children == nil {
		n.children = make(map[string]*node)
	}
	var ch *node
	var ok bool
	if ch, ok = n.children[name]; !ok {
		ch = newNode(name, unsubscribe)
		n.children[name] = ch
	}

	if !ch.unsubscribe {
		n.unsubscribe = false
	}

	ch.add(names[1:], unsubscribe)
}

func (n *node) prettyPrint(w io.Writer, indent int) {
	pad := strings.Repeat(" ", indent)

	if len(n.children) != 0 {
		fmt.Fprintf(w, "%s|--- %s (%d messages in %d threads; unsubscribe: %v)\n",
			pad, n.name, n.count, len(n.children), n.unsubscribe)
	} else {
		fmt.Fprintf(w, "%s|--- %s (%d messages; unsubscribe: %v)\n",
			pad, n.name, n.count, n.unsubscribe)
	}

	if len(n.children) == 0 {
		return
	}

	var nodes []*node
	for _, n := range n.children {
		nodes = append(nodes, n)
	}
	sort.Slice(nodes,
		func(i, j int) bool {
			return nodes[i].count > nodes[j].count
		})

	for _, ch := range nodes {
		ch.prettyPrint(w, indent+5)
	}
}

type emailTree struct {
	root node
}

func (t *emailTree) add(record []string) error {
	fromAddr := record[2]
	unsubscribe := record[8]

	from, err := mail.ParseAddress(fromAddr)
	if err != nil {
		return err
	}

	parts := strings.Split(from.Address, "@")
	var names []string
	for i := len(parts) - 1; i >= 0; i-- {
		names = append(names, parts[i])
	}
	names = append(names, from.Name)

	t.root.add(names, unsubscribe)
	return nil
}

func (t *emailTree) prettyPrint(w io.Writer, indent int) {
	t.root.prettyPrint(w, indent)
}

func showEmailTree() error {
	var tree emailTree

	err := iterateCSVFile(*argInputFile,
		func(record []string) bool {
			tree.add(record)
			return true
		})

	tree.prettyPrint(os.Stdout, 0)
	return err
}

// The following parts are from google's gmail API example app.

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}
